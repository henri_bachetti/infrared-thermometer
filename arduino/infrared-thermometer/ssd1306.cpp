
#include "ssd1306.h"

MySSD1306::MySSD1306(void) :
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c()
#else
  Adafruit_SSD1306(-1)
#endif
{
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::setFont(System5x7);
#endif
}

void MySSD1306::begin(void)
{
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::begin(&Adafruit128x64, 0x3C);
#else
  Adafruit_SSD1306::begin(SSD1306_SWITCHCAPVCC, 0x3C);
#endif
}

void MySSD1306::clearDisplay(void)
{
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::clear();
#else
  Adafruit_SSD1306::clearDisplay();
#endif
}

void MySSD1306::setTextSize(uint8_t s)
{
#ifdef SSD1306_ASCII
  if (s == 1) {
    SSD1306AsciiAvrI2c::set1X();
  }
  else {
    SSD1306AsciiAvrI2c::set2X();
  }
#else
  Adafruit_SSD1306::setTextSize(s);
#endif
}

void MySSD1306::setTextColor(uint16_t c)
{
#ifdef SSD1306_ASCII
#else
  Adafruit_SSD1306::setTextColor(c);
#endif
}

void MySSD1306::setCursorXY(uint8_t x, uint8_t y)
{
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::setCursor(x, y / 8);
#else
  Adafruit_SSD1306::setCursor(x, y);
#endif
}

void MySSD1306::display(void)
{
#ifdef SSD1306_ASCII
#else
  Adafruit_SSD1306::display();
#endif
}

void MySSD1306::print(const char *s, int y, int size)
{
  setTextSize(size);
  setCursorXY((SCREEN_WIDTH - (strlen(s) * 6 * size)) / 2, y);
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::print(s);
#else
  Adafruit_SSD1306::print(s);
#endif
}

void MySSD1306::print(const char *s, int x, int y, int size)
{
  setTextSize(size);
  setCursorXY(x, y);
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::print(s);
#else
  Adafruit_SSD1306::print(s);
#endif
}

void MySSD1306::print(char c, int x, int y, int size)
{
  setTextSize(size);
  setCursorXY(x, y);
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::print(c);
#else
  Adafruit_SSD1306::print(c);
#endif
}

void MySSD1306::print(int n, int x, int y, int size)
{
  setTextSize(size);
  setCursorXY(x, y);
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::print(n);
#else
  Adafruit_SSD1306::print(n);
#endif
}

void MySSD1306::print(const __FlashStringHelper *s, int y, int size)
{
  char buf[MAX_LINE_LENGTH];

  PGM_P p = reinterpret_cast<PGM_P>(s);
  size_t n = 0;
  while (n < MAX_LINE_LENGTH) {
    unsigned char c = pgm_read_byte(p++);
    buf[n++] = c;
    if (c == 0) break;
  }
  buf[n] = 0;
  setTextSize(size);
  setCursorXY((SCREEN_WIDTH - (strlen(buf) * 6 * size)) / 2, y);
#ifdef SSD1306_ASCII
  SSD1306AsciiAvrI2c::print(buf);
#else
  Adafruit_SSD1306::print(buf);
#endif
}

#ifdef SSD1306_ASCII

GLCDFONTDECL(Batt6x14) = {
  0x0, 0x0, // size of zero indicates fixed width font,
  6,     // width
  14,     // height
  0x20,
  6,
  //0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X20, 0X20, 0X20, 0X20, 0X20, 0X20
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // space
  0xFF, 0x01, 0x01, 0x01, 0x01, 0xFF, 0x3F, 0x20, 0x20, 0x20, 0x20, 0x3F, // Battery Empty
  0xFF, 0x01, 0x01, 0x01, 0x01, 0xFF, 0x3F, 0x38, 0x38, 0x38, 0x38, 0x3F, // Battery 20%
  0xFF, 0x01, 0x01, 0x01, 0x01, 0xFF, 0x3F, 0x3f, 0x3f, 0x3f, 0x3f, 0x3F, // Battery 40%
  0xFF, 0xc1, 0xc1, 0xc1, 0xc1, 0xFF, 0x3F, 0x3f, 0x3f, 0x3f, 0x3f, 0x3F, // Battery 60%
  0xFF, 0xf1, 0xf1, 0xf1, 0xf1, 0xFF, 0x3F, 0x3f, 0x3f, 0x3f, 0x3f, 0x3F, // Battery 80%
  0xFf, 0xff, 0xff, 0xff, 0xff, 0xFf, 0x3F, 0x3f, 0x3f, 0x3f, 0x3f, 0x3F, // Battery 100%
};

#endif

void MySSD1306::displayBatteryLevel(int batteryLevel, int x, int y)
{
  static bool flash = false;
  if (flash || batteryLevel > 10) {
#ifdef SSD1306_ASCII
    setFont(Batt6x14);
    print((char)(' ' + 1+ round(batteryLevel / 20)), x, y, 1);
    setFont(System5x7);
  }
  else {
    setFont(Batt6x14);
    print(' ', x, y, 1);
    setFont(System5x7);
  }
#else
    drawRect(x, y, 6, 16, WHITE);
    fillRect(x, y + ((16 - batteryLevel / 6)), 6, batteryLevel / 6, WHITE);
  }
#endif
  flash = !flash;
}
