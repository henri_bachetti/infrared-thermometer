
#ifndef __SSD1306_H__
#define __SSD1306_H__

// use the Greiman library instead of AdaFruit
#define SSD1306_ASCII

#ifndef SSD1306_ASCII
#include <Adafruit_SSD1306.h>
#define DEGREE 247
#else
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"
#define BLACK 0
#define WHITE 0
#define DEGREE 128
#endif

#define SCREEN_WIDTH        128
#define SCREEN_HEIGHT       64
#define MAX_LINE_LENGTH     22

#ifndef SSD1306_ASCII
class MySSD1306 : public Adafruit_SSD1306
#else
class MySSD1306 : public SSD1306AsciiAvrI2c
#endif
{
  public:
    MySSD1306(void);
    void begin(void);
    void clearDisplay(void);
    void setTextSize(uint8_t s);
    void setTextColor(uint16_t c);
    void setCursorXY(uint8_t x, uint8_t y);
    void display(void);
    void print(const char *s, int y, int size);
    void print(const char *s, int x, int y, int size);
    void print(char c, int x, int y, int size);
    void print(int n, int x, int y, int size);
    void print(const __FlashStringHelper *s, int y, int size);
    void displayBatteryLevel(int batteryLevel, int x, int y);
};

#endif

