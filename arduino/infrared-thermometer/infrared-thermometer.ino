

#define NO_SENSOR       0           // no sensor for test purpose only
#define ADAFRUIT        1           // use ADAFRUIT only when no emissivity tuning is required
#define SPARKFUN        2           // SPARKFUN : the best
#define SENSOR_LIBRARY  SPARKFUN
//#define MY_DEBUG

#include <Wire.h>
#include <EEPROM.h>
#include <LowPower.h>
#if SENSOR_LIBRARY == ADAFRUIT
#include <Adafruit_MLX90614.h>
#elif SENSOR_LIBRARY == SPARKFUN
#include <SparkFunMLX90614.h>
#endif

#include "ssd1306.h"

#define PLUS_BUTTON_PIN     3
#define MINUS_BUTTON_PIN    4
#define MODE_BUTTON_PIN     5
#define DEFAULT_EMISSIVITY  95
#define VREF                1.15
#define DIVIDER             0.248

struct batteryCapacity
{
  float voltage;
  int capacity;
};

const batteryCapacity remainingCapacity[] = {
  4.20,   100,
  4.10,   96,
  4.00,   92,
  3.96,   89,
  3.92,   85,
  3.89,   81,
  3.86,   77,
  3.83,   73,
  3.80,   69,
  3.77,   65,
  3.75,   62,
  3.72,   58,
  3.70,   55,
  3.66,   51,
  3.62,   47,
  3.58,   43,
  3.55,   40,
  3.51,   35,
  3.48,   32,
  3.44,   26,
  3.40,   24,
  3.37,   20,
  3.35,   17,
  3.27,   13,
  3.20,   9,
  3.1,    6,
  3.00,   3,
};

const int ncell = sizeof(remainingCapacity) / sizeof(struct batteryCapacity);

unsigned int getBatteryCapacity(void)
{
  unsigned int adc = analogRead(0);
#ifdef MY_DEBUG
  Serial.print("ADC: ");
  Serial.println(adc);
#endif
  float voltage = adc * VREF / 1023 / DIVIDER;
#ifdef MY_DEBUG
  Serial.print("VCC: ");
  Serial.println(voltage, 3);
#endif
  for (int i = 0 ; i < ncell ; i++) {
#ifdef MY_DEBUG
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(remainingCapacity[i].voltage);
    Serial.print(" | ");
    Serial.println(remainingCapacity[i].capacity);
#endif
    if (voltage > remainingCapacity[i].voltage) {
      return remainingCapacity[i].capacity;
    }
  }
  return 0;
}

int sprintf_F(char *str, const __FlashStringHelper *format, ...)
{
  char fmt[MAX_LINE_LENGTH];

  va_list ap;
  va_start (ap, format);
  PGM_P p = reinterpret_cast<PGM_P>(format);
  size_t n = 0;
  while (1) {
    unsigned char c = pgm_read_byte(p++);
    fmt[n++] = c;
    if (c == 0) break;
  }
  fmt[n] = 0;
  va_end (ap);
  return (vsnprintf(str, MAX_LINE_LENGTH, fmt, ap));
}

MySSD1306 display;
#if SENSOR_LIBRARY == ADAFRUIT
Adafruit_MLX90614 mlx = Adafruit_MLX90614();
#elif SENSOR_LIBRARY == SPARKFUN
IRTherm mlx;
#endif

#define MAGIC     0xDEADBEEF
#define EEPROM_ADDR 0

struct __attribute__ ((packed)) eeprom_data
{
  int emissivity;
  unsigned long magic;
};

struct eeprom_data eepromData;

void setup()
{
  struct eeprom_data data;

  Serial.begin(115200);
  Serial.println(F("MLX90614 thermometer"));
  analogReference(INTERNAL);
  pinMode(MODE_BUTTON_PIN, INPUT_PULLUP);
  pinMode(PLUS_BUTTON_PIN, INPUT_PULLUP);
  pinMode(MINUS_BUTTON_PIN, INPUT_PULLUP);

  EEPROM.get(EEPROM_ADDR, data);
  if (data.magic != MAGIC) {
    Serial.println(F("No configuration available"));
    eepromData.emissivity = DEFAULT_EMISSIVITY;
    eepromData.magic = MAGIC;
    EEPROM.put(EEPROM_ADDR, eepromData);
  }
  else {
    memcpy(&eepromData, &data, sizeof(data));
  }
  display.begin();
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.clearDisplay();
#if SENSOR_LIBRARY == ADAFRUIT
  mlx.begin();
#elif SENSOR_LIBRARY == SPARKFUN
  float emissivity = mlx.readEmissivity();
  Serial.print(F("EMISSIVITY: ")); Serial.println(emissivity);
#endif
}

int plusButtonState = HIGH;
int minusButtonState = HIGH;

void loop()
{
  static unsigned long timePressed;
  static int plusButtonPrev;
  static int minusButtonPrev;
  int mode = digitalRead(MODE_BUTTON_PIN);
  int plusButtonState = digitalRead(PLUS_BUTTON_PIN);
  int minusButtonState = digitalRead(MINUS_BUTTON_PIN);
  char temp[10];
  char text[MAX_LINE_LENGTH];

  if (plusButtonState != plusButtonPrev || minusButtonState != minusButtonPrev) {
    if (plusButtonState == LOW || minusButtonState == LOW) {
      // plus or minus button pressed
      timePressed = millis();
    } else {
      // plus or minus button released
#if SENSOR_LIBRARY == SPARKFUN
      float emissivity = mlx.readEmissivity();
      if (round(emissivity * 100) != eepromData.emissivity) {
        Serial.print(F("REGISTER EMISSIVITY ")); Serial.println(eepromData.emissivity);
        EEPROM.put(EEPROM_ADDR, eepromData);
        mlx.setEmissivity((float)eepromData.emissivity / 100);
      }
#endif
    }
  }
  plusButtonPrev = plusButtonState;
  minusButtonPrev = minusButtonState;
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
#ifndef SSD1306_ASCII
  display.clearDisplay();
#endif
  if (mode == HIGH) {
    display.print(F("THERMOMETRE"), 0, 1);
    display.print(F("INFRAROUGE"), 8, 1);
  } else {
    display.print(F("INFRARED"), 0, 1);
    display.print(F("THERMOMETER"), 8, 1);
  }
  if (plusButtonState == LOW || minusButtonState == LOW) {
    // plus or minus button pressed
    if (plusButtonState == LOW) {
      eepromData.emissivity = min(eepromData.emissivity++, 100);
    }
    if (minusButtonState == LOW) {
      eepromData.emissivity = max(eepromData.emissivity--, 0);
    }
    Serial.print(F("EMISSIVITY ")); Serial.println(eepromData.emissivity);
    if (mode == HIGH) {
      sprintf_F(text, F("EMISSIVITE: %d"), eepromData.emissivity);
    } else {
      sprintf_F(text, F("EMISSIVITY: %d"), eepromData.emissivity);
    }
    display.print(text, 30, 1);
    if (millis() - timePressed >= 5000) {
      delay(100);
    }
    else {
      delay(500);
    }
  } else {
    float ambient;
    float object;
#if SENSOR_LIBRARY == ADAFRUIT
    if (mode == HIGH) {
      ambient = mlx.readAmbientTempC();
      object = mlx.readObjectTempC();
    } else {
      ambient = mlx.readAmbientTempF();
      object = mlx.readObjectTempF();
    }
#elif SENSOR_LIBRARY == SPARKFUN
    if (mode == HIGH) {
      mlx.setUnit(TEMP_C);
    } else {
      mlx.setUnit(TEMP_F);
    }
    if (mlx.read()) {
      ambient = mlx.ambient();
      object = mlx.object();
    }
    else {
      Serial.println(F("ERROR "));
      display.print(F("SENSOR ERROR"), 30, 1);
      delay(1000);
      return;
    }
#else
    ambient = 20.0;
    object = 50.0;
#endif
    Serial.print(object);  Serial.print(F(",")); Serial.println(ambient);
    dtostrf(ambient, 2, 2, temp);
    if (mode == HIGH) {
      sprintf_F(text, F("AMBIANT: %s%cC"), temp, DEGREE);
    } else {
      sprintf_F(text, F("AMBIENT: %s%cF"), temp, DEGREE);
    }
    display.print(text, 20, 1);
    if (mode == HIGH) {
      sprintf_F(text, F("EMISSIVITE: %d"), eepromData.emissivity);
    } else {
      sprintf_F(text, F("EMISSIVITY: %d"), eepromData.emissivity);
    }
    display.print(text, 30, 1);
    dtostrf(object, 2, 2, temp);
    if (mode == HIGH) {
      sprintf_F(text, F("%s%cC"), temp, DEGREE);
    }
    else {
      sprintf_F(text, F("%s%cF"), temp, DEGREE);
    }
    display.print(text, 50, 2);
    int batteryLevel = getBatteryCapacity();
    Serial.print(F("battery: ")); Serial.println(batteryLevel);
    if (batteryLevel > 0) {
      display.displayBatteryLevel(batteryLevel, 0, 0);
    }
    display.display();
    delay(20);
    LowPower.powerDown(SLEEP_500MS, ADC_OFF, BOD_OFF);
  }
}

