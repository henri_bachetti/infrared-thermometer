# INFRA-RED THERMOMETER

The purpose of this page is to explain step by step the realization of an infra-red thermometer ARDUINO NANO.

The board uses the following components :

 * an ARDUINO NANO
 * a MELEXIS MLX90614ESF
 * a small OLED 0.96" display
 * some passive components
 * the board is powered by the USB or a 18560 LITHIUM battery and a stepup converter.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/11/thermometre-infrarouge.html


