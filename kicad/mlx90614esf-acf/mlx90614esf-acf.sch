EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:mlx90614esf-baa-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X06 P1
U 1 1 5DED0653
P 4750 3150
F 0 "P1" H 4750 3500 50  0000 C CNN
F 1 "THERMOMETER" V 4850 3150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 4750 3150 50  0001 C CNN
F 3 "" H 4750 3150 50  0000 C CNN
	1    4750 3150
	-1   0    0    1   
$EndComp
$Comp
L Led_Small D1
U 1 1 5DED069B
P 5400 3500
F 0 "D1" H 5250 3550 50  0000 L CNN
F 1 "LASER" H 5300 3400 50  0000 L CNN
F 2 "LEDs:LED-3MM" V 5400 3500 50  0001 C CNN
F 3 "" V 5400 3500 50  0000 C CNN
	1    5400 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 2900 5950 2900
Wire Wire Line
	4950 3000 5950 3000
Wire Wire Line
	4950 3100 5950 3100
Wire Wire Line
	5200 3200 5200 3800
Wire Wire Line
	4950 3200 5950 3200
Wire Wire Line
	4950 3400 5250 3400
Wire Wire Line
	5250 3400 5250 3700
Wire Wire Line
	5400 3600 5400 3700
Wire Wire Line
	5400 3400 5400 3300
$Comp
L GND #PWR01
U 1 1 5DEE21D8
P 5200 3800
F 0 "#PWR01" H 5200 3550 50  0001 C CNN
F 1 "GND" H 5200 3650 50  0000 C CNN
F 2 "" H 5200 3800 50  0000 C CNN
F 3 "" H 5200 3800 50  0000 C CNN
	1    5200 3800
	1    0    0    -1  
$EndComp
Connection ~ 5200 3200
Wire Wire Line
	5400 3300 4950 3300
Wire Wire Line
	5400 3700 5250 3700
$Comp
L MLX90614 U1
U 1 1 5DEF7B5D
P 5950 2900
F 0 "U1" H 5300 2850 60  0000 C CNN
F 1 "MLX90614" H 5300 2750 60  0000 C CNN
F 2 "myTransistors:TO-39-4" H 5950 2900 60  0001 C CNN
F 3 "" H 5950 2900 60  0000 C CNN
	1    5950 2900
	-1   0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5DEF7CEC
P 5300 2700
F 0 "C1" V 5350 2750 50  0000 L CNN
F 1 "0.1µF" V 5450 2600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 5338 2550 50  0001 C CNN
F 3 "" H 5300 2700 50  0000 C CNN
	1    5300 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 2700 5100 2700
Wire Wire Line
	5100 2700 5100 3100
Connection ~ 5100 3100
Wire Wire Line
	5450 2700 5500 2700
Wire Wire Line
	5500 2700 5500 3200
Connection ~ 5500 3200
$Comp
L R R1
U 1 1 5DEF7DB6
P 5650 3450
F 0 "R1" V 5730 3450 50  0000 C CNN
F 1 "10K" V 5650 3450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5580 3450 50  0001 C CNN
F 3 "" H 5650 3450 50  0000 C CNN
	1    5650 3450
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5DEF7E0A
P 5850 3450
F 0 "R2" V 5930 3450 50  0000 C CNN
F 1 "10K" V 5850 3450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5780 3450 50  0001 C CNN
F 3 "" H 5850 3450 50  0000 C CNN
	1    5850 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5DEF7E98
P 5750 3800
F 0 "#PWR02" H 5750 3550 50  0001 C CNN
F 1 "GND" H 5750 3650 50  0000 C CNN
F 2 "" H 5750 3800 50  0000 C CNN
F 3 "" H 5750 3800 50  0000 C CNN
	1    5750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3700 5850 3600
Wire Wire Line
	5650 3700 5850 3700
Wire Wire Line
	5650 3700 5650 3600
Wire Wire Line
	5750 3700 5750 3800
Connection ~ 5750 3700
Wire Wire Line
	5650 3300 5650 3000
Connection ~ 5650 3000
Wire Wire Line
	5850 3300 5850 2900
Connection ~ 5850 2900
$EndSCHEMATC
