EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:mlx90614esf-baa-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X06 P1
U 1 1 5DED0653
P 4800 3150
F 0 "P1" H 4800 3500 50  0000 C CNN
F 1 "THERMOMETER" V 4900 3150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 4800 3150 50  0001 C CNN
F 3 "" H 4800 3150 50  0000 C CNN
	1    4800 3150
	-1   0    0    1   
$EndComp
$Comp
L Led_Small D1
U 1 1 5DED069B
P 5400 3500
F 0 "D1" H 5250 3550 50  0000 L CNN
F 1 "LASER" H 5300 3400 50  0000 L CNN
F 2 "LEDs:LED-3MM" V 5400 3500 50  0001 C CNN
F 3 "" V 5400 3500 50  0000 C CNN
	1    5400 3500
	0    -1   -1   0   
$EndComp
$Comp
L MLX90614-MODULE U1
U 1 1 5DED0ACA
P 5600 2900
F 0 "U1" H 4950 2850 60  0000 C CNN
F 1 "MLX90614-MODULE" H 5075 3050 60  0000 C CNN
F 2 "myModules:MLX90614-MODULE" H 5600 2900 60  0001 C CNN
F 3 "" H 5600 2900 60  0000 C CNN
	1    5600 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 2900 5300 2900
Wire Wire Line
	5300 2900 5300 3000
Wire Wire Line
	5300 3000 5600 3000
Wire Wire Line
	5600 2900 5400 2900
Wire Wire Line
	5400 2900 5400 2950
Wire Wire Line
	5400 2950 5200 2950
Wire Wire Line
	5200 2950 5200 3000
Wire Wire Line
	5200 3000 5000 3000
Wire Wire Line
	5600 3100 5400 3100
Wire Wire Line
	5400 3100 5400 3150
Wire Wire Line
	5400 3150 5200 3150
Wire Wire Line
	5200 3150 5200 3800
Wire Wire Line
	5200 3200 5000 3200
Wire Wire Line
	5000 3100 5300 3100
Wire Wire Line
	5300 3100 5300 3200
Wire Wire Line
	5300 3200 5600 3200
Wire Wire Line
	5000 3400 5250 3400
Wire Wire Line
	5250 3400 5250 3700
Wire Wire Line
	5400 3600 5400 3700
Wire Wire Line
	5400 3400 5400 3300
$Comp
L GND #PWR01
U 1 1 5DEE21D8
P 5200 3800
F 0 "#PWR01" H 5200 3550 50  0001 C CNN
F 1 "GND" H 5200 3650 50  0000 C CNN
F 2 "" H 5200 3800 50  0000 C CNN
F 3 "" H 5200 3800 50  0000 C CNN
	1    5200 3800
	1    0    0    -1  
$EndComp
Connection ~ 5200 3200
Wire Wire Line
	5400 3300 5000 3300
Wire Wire Line
	5400 3700 5250 3700
$EndSCHEMATC
